# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit postgresql
KEYWORDS="~x86"

DESCRIPTION="A database-independent, system-independent benchmark to enable individuals to analyze the performance of a variety of database and system configurations."
HOMEPAGE="http://osdb.sourceforge.net"
SRC_URI="mirror://sourceforge/osdb/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
IUSE="mysql postgres"

DEPEND="mysql? ( dev-db/mysql )
		postgres? ( virtual/postgresql-base )"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	find . -iname "Makefile" | xargs \
		sed -i \
			-e 's:$(bindir):$(DESTDIR)/$(bindir):g' \
			|| die "sed failed"
}

src_compile() {
	econf \
		$(use_with mysql) \
		$(use_with postgres postgresql $(postgresql_get_pgconfig )) \
		|| die "econf failed"
	emake || die "emake failed"

# TODO: check how mpi works and enable the support
# mpicc is needed to compile
#	if use mpi ; then
#		cd src/
#		emake mpi || die "emake mpi failed"
#	fi
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS ChangeLog README* TODO
	dohtml docs/*
}
