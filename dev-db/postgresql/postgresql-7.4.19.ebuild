# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit versionator

DESCRIPTION="Meta package for PostgreSQL sophisticated and powerful Object-Relational DBMS"
HOMEPAGE="http://www.postgresql.org/"

LICENSE="POSTGRESQL"
SLOT="$(get_version_component_range 1-2)"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~sparc-fbsd ~x86 ~x86-fbsd"
IUSE=""

RDEPEND="~dev-db/postgresql-base-${PV}
	~dev-db/postgresql-server-${PV}"

pkg_setup() {
	ewarn "This ebuild is a temporary replacement for dev-db/postgresql to maintain"
	ewarn "compatibility between the old- and new-style postgresql package structure."
	ewarn "It will vanish as soon as the new-style ebuilds are marked stable."
	ewarn "To install postgresql, please use:"
	ewarn " 'emerge dev-db/postgresql-server' and/or"
	ewarn " 'emerge dev-db/postgresql-base' directly."
}
