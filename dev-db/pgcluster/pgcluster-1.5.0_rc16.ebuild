# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

WANT_AUTOCONF="latest"
WANT_AUTOMAKE="none"
inherit eutils toolchain-funcs versionator autotools

DESCRIPTION="The synchronous replication system of the multi-master composition for PostgresSQL"
HOMEPAGE="http://www.csra.co.jp/~mitani/jpug/pgcluster/en/ http://pgfoundry.org/projects/pgcluster/"
SRC_URI="http://pgfoundry.org/frs/download.php/1295/${P/_/}.tar.gz"

LICENSE="POSTGRESQL"
SLOT="8.1"
KEYWORDS="~x86 ~ppc ~sparc ~mips ~alpha ~arm ~hppa ~amd64 ~ia64 ~s390 ~ppc64"
IUSE_LINGUAS="
	linguas_af linguas_cs linguas_de linguas_es linguas_fa linguas_fr
	linguas_hr linguas_hu linguas_it linguas_ko linguas_nb linguas_pl
	linguas_pt_BR linguas_ro linguas_ru linguas_sk linguas_sl linguas_sv
	linguas_tr linguas_zh_CN linguas_zh_TW"
IUSE="ssl perl python tcl xml kerberos nls pam pg-intdatetime readline ssl threads zlib eselect kernel_linux ${IUSE_LINGUAS}"

wanted_languages() {
	for u in ${IUSE_LINGUAS} ; do
		use $u && echo ${u#linguas_}
	done
}

RDEPEND="
	kerberos? ( virtual/krb5 )
	pam? ( virtual/pam )
	readline? ( >=sys-libs/readline-4.1 )
	ssl? ( >=dev-libs/openssl-0.9.6-r1 )
	zlib? ( >=sys-libs/zlib-1.1.3 )
	>=app-admin/eselect-postgresql-0.1
	perl? ( >=dev-lang/perl-5.6.1-r2 )
	python? ( >=dev-lang/python-2.2 dev-python/egenix-mx-base )
	tcl? ( >=dev-lang/tcl-8 )
	xml? ( dev-libs/libxml2 dev-libs/libxslt )
	!<dev-db/libpq-99.99.99
	"
DEPEND="${RDEPEND}
	>=sys-devel/bison-1.875
	nls? ( sys-devel/gettext )
	xml? ( dev-util/pkgconfig )"

[[ -r /etc/conf.d/${PN}-${SLOT} ]] && source /etc/conf.d/${PN}-${SLOT}
[[ -z "${PGDATA}" ]] && PGDATA="/var/lib/${PN}/${SLOT}/data"
[[ -z "${PG_MAX_CONNECTIONS}" ]] && PG_MAX_CONNECTIONS="512"

S="${WORKDIR}/${P/_/}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	epatch "${FILESDIR}/postgresql-${SLOT}-nls.patch"
	epatch "${FILESDIR}/postgresql-${SLOT}-sh-spinlocks.patch"
	mkdir -p "${S}/src/test/regress"
	echo "all install:" > "${S}/src/test/regress/GNUmakefile"

	eautoconf
}

src_compile() {
	econf --prefix=/usr/$(get_libdir)/${PN}-${SLOT} \
		--datadir=/usr/share/${PN}-${SLOT} \
		--sysconfdir=/etc/${PN}-${SLOT} \
		--includedir=/usr/include/${PN}-${SLOT} \
		--with-locale-dir=/usr/share/${PN}-${SLOT} \
		--mandir=/usr/share/${PN}-${SLOT}/man \
		--with-docdir=/usr/share/doc/${PF} \
		$(use_with perl) \
		$(use_with python) \
		$(use_with tcl) \
		$(use_with kerberos krb5) \
		"$(use_enable nls nls "$(wanted_languages)")" \
		$(use_with pam) \
		$(use_with pg-intdatetime integer-datetimes) \
		$(use_with readline) \
		$(use_with ssl openssl) \
		$(use_enable threads thread-safety) \
		$(use_with zlib) \
		|| die "econf failed"

	for bd in . contrib $(use xml && echo contrib/xml2) ; do
		emake -C $bd LD="$(tc-getLD) $(get_abi_LDFLAGS)" || die "emake in $bd failed"
	done
}

src_install() {
	if use perl ; then
		mv -f "${S}/src/pl/plperl/GNUmakefile" "${S}/src/pl/plperl/GNUmakefile_orig"
		sed -e "s:\$(DESTDIR)\$(plperl_installdir):\$(plperl_installdir):" \
			"${S}/src/pl/plperl/GNUmakefile_orig" > "${S}/src/pl/plperl/GNUmakefile"
	fi

	for bd in . contrib $(use xml && echo contrib/xml2) ; do
		emake -C $bd DESTDIR="${D}" install || die "emake install in $bd failed"
	done

	insinto /usr/include/${PN}-${SLOT}/postmaster
	doins "${S}"/src/include/postmaster/*.h

	dodoc COPYRIGHT HISTORY INSTALL* README* doc/{FAQ*,KNOWN_BUGS,MISSING_FEATURES,README*,TODO,bug.template}

	dodir /usr/lib/eselect-postgresql/slots/${SLOT}-${PN}
	{
		echo "postgres_ebuilds=\"\${postgres_ebuilds} ${PF}\""
		echo "postgres_prefix=/usr/$(get_libdir)/${PN}-${SLOT}"
		echo "postgres_datadir=/usr/share/${PN}-${SLOT}"
		echo "postgres_bindir=/usr/$(get_libdir)/${PN}-${SLOT}/bin"
		echo "postgres_service=\"pgcluster-${SLOT}\""
		echo "postgres_symlinks=("
			IDIR="/usr/include/${PN}-${SLOT}"
			echo " " ${IDIR} /usr/include/postgresql
			echo " " {${IDIR},/usr/include}/libpq-fe.h
			echo " " {${IDIR},/usr/include}/libpq
			echo " " {${IDIR},/usr/include}/postgres_ext.h
		echo ")"
	} > "${D}/usr/lib/eselect-postgresql/slots/${SLOT}-${PN}/${PN}"
	cat >"${T}"/55${PN}-95-${SLOT} <<-__EOF__
		LDPATH=/usr/$(get_libdir)/${PN}-${SLOT}/$(get_libdir)
		MANPATH=/usr/share/${PN}-${SLOT}/man
	__EOF__
	doenvd "${T}/55${PN}-96-${SLOT}"

	for i in ${PN} pglb pgreplicate ; do
		newinitd "${FILESDIR}/${i}.init-${SLOT}" ${i}-${SLOT} || die "Inserting $i init.d-file failed"
		newconfd "${FILESDIR}/${i}.conf-${SLOT}" ${i}-${SLOT} || die "Inserting $i conf.d-file failed"
	done
}

pkg_setup() {
	if [[ -f "${PGDATA}/PG_VERSION" ]] ; then
		if [[ $(cat "${PGDATA}/PG_VERSION") != ${SLOT} ]] ; then
			eerror "PostgreSQL ${PV} cannot upgrade your existing databases, you must"
			eerror "use pg_dump to export your existing databases to a file, and then"
			eerror "pg_restore to import them when you have upgraded completely."
			eerror "You must remove your entire database directory to continue."
			eerror "(database directory = ${PGDATA})."
			die "Remove your database directory to continue"
		fi
	fi

	local ff=""
	for f in "${ROOT}"/usr/include/{postgresql,libpq-fe.h,libpq,postgres_ext.h} ; do
		[[ -e "${f}" ]] || continue
		[[ -L "${f}" ]] && continue
		if [[ -d "${f}" ]] ; then
			if [[ -z "$(find "${f}" -not \( -type l -or -type d \))" ]] ; then
				rm -rf "${f}"
			else
				ff="$ff $f"
			fi
		else
			ff="$ff $f"
		fi
	done
	if [[ ! -z "$ff" ]] ; then
		eerror "You have leftovers from previous installations that can't be"
		eerror "dealt with automatically. The proper way to treat files is:"
		eerror ""
		eerror "rm -rf ${ff}"
		eerror ""
		die "Please, remove the files manually"
	fi

	enewgroup postgres 70
	enewuser postgres 70 /bin/bash /var/lib postgres
}

pkg_postinst() {
	use eselect && eselect postgresql set-all ${SLOT}
	eselect postgresql update
	elog "If you need a global psqlrc-file, you can place it in '${ROOT}/etc/${PN}-${SLOT}/'."
	elog
	elog "Execute the following command to setup the initial database environment:"
	elog
	elog "emerge --config =${CATEGORY}/${PF}"
	elog
	elog "The autovacuum function, which was in contrib, has been moved to the main"
	elog "PostgreSQL functions starting with 8.1."
	elog "You can enable it in ${ROOT}/etc/${PN}-${SLOT}/postgresql.conf."
}

pkg_prerm() {
	[[ "$(eselect postgresql show)" = "${SLOT}-${PN}" ]] && eselect postgresql reset
}

pkg_postrm() {
	eselect postgresql update
}

pkg_config() {
	einfo "Creating the data directory ..."
	mkdir -p "${PGDATA}"
	chown -Rf postgres:postgres "${PGDATA}"
	chmod 0700 "${PGDATA}"

	einfo "Initializing the database ..."
	if [[ -f "${PGDATA}/PG_VERSION" ]] ; then
		eerror "PostgreSQL ${PV} cannot upgrade your existing databases."
		eerror "You must remove your entire database directory to continue."
		eerror "(database directory = ${PGDATA})."
		die "Remove your database directory to continue"
	else
		if use kernel_linux ; then
			local SEM=`sysctl -n kernel.sem | cut -f-3`
			local SEMMNI=`sysctl -n kernel.sem | cut -f4`
			local SEMMNI_MIN=`expr \( ${PG_MAX_CONNECTIONS} + 15 \) / 16`
			local SHMMAX=`sysctl -n kernel.shmmax`
			local SHMMAX_MIN=`expr 500000 + 30600 \* ${PG_MAX_CONNECTIONS}`

			if [ ${SEMMNI} -lt ${SEMMNI_MIN} ] ; then
				eerror "The current value of SEMMNI is too low"
				eerror "for PostgreSQL to run ${PG_MAX_CONNECTIONS} connections!"
				eerror "Temporary setting this value to ${SEMMNI_MIN} while creating the initial database."
				echo ${SEM} ${SEMMNI_MIN} > /proc/sys/kernel/sem
			fi
			su postgres -c "/usr/$(get_libdir)/${PN}-${SLOT}/bin/initdb --pgdata ${PGDATA} ${PG_INITDB_OPTS}"

			if [ ! `sysctl -n kernel.sem | cut -f4` -eq ${SEMMNI} ] ; then
				echo ${SEM} ${SEMMNI} > /proc/sys/kernel/sem
				ewarn "Restoring the SEMMNI value to the previous value."
				ewarn "Please edit the last value of kernel.sem in /etc/sysctl.conf"
				ewarn "and set it to at least ${SEMMNI_MIN}:"
				ewarn
				ewarn "  kernel.sem = ${SEM} ${SEMMNI_MIN}"
				ewarn
			fi

			if [ ${SHMMAX} -lt ${SHMMAX_MIN} ] ; then
				eerror "The current value of SHMMAX is too low for postgresql to run."
				eerror "Please edit /etc/sysctl.conf and set this value to at least ${SHMMAX_MIN}:"
				eerror
				eerror "  kernel.shmmax = ${SHMMAX_MIN}"
				eerror
			fi
		else
			su postgres -c "/usr/$(get_libdir)/${PN}-${SLOT}/bin/initdb --pgdata ${PGDATA} ${PG_INITDB_OPTS}"
		fi

		einfo
		einfo "You can use the '${ROOT}/etc/init.d/${PN}-${SLOT}' script to run pgcluster instead of 'pg_ctl'."
		einfo
	fi
}
