# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

#					 min max
WANT_POSTGRES_SLOTS="8.0 8.2"
inherit eutils postgresql-ext
DESCRIPTION="Indexable IPv4 range / CIDR type for PostgreSQL"
HOMEPAGE="http://ip4r.projects.postgresql.org/"
SRC_URI="http://pgfoundry.org/frs/download.php/1226/ip4r-${PV}.tar.gz"
LICENSE="POSTGRESQL"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

S="${WORKDIR}/ip4r-${PV}"

pgslot_src_compile() {
	emake USE_PGXS=1 || die "emake for slot $SLOTSLOT failed"
}

pgslot_src_install() {
	emake install USE_PGXS=1 DESTDIR="${D}" \
		docdir="/usr/share/doc/${P}" \
		|| die "emake install for slot $SLOTSLOT failed"
}
