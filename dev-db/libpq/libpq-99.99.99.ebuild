# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"

inherit eutils

DESCRIPTION="Dummy ebuild provided to satisfy dependency"
HOMEPAGE="http://overlays.gentoo.org/proj/postgresql/"

LICENSE="POSTGRESQL"
SLOT="4"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~sparc-fbsd ~x86 ~x86-fbsd"
IUSE="fixme threads"

DEPEND="!dev-db/libpq:3"
RDEPEND="${DEPEND}"
PDEPEND="dev-db/postgresql-base"

pkg_setup() {
	if ! use fixme ; then
		ewarn "You should add fixme USE-flag to enable this ebuild"
		ewarn "This is the way you should express your awareness of"
		ewarn "this ebuild being a dirty hack. It's in experimental"
		ewarn "tree, isn't it?"
		die "Fix me!"
	fi
}

pkg_postinst() {
	if [[ "$(eselect postgresql show)" = "(none)" ]] ; then
		# You may think this is a dirty hack. But this whole ebuild is.
		eselect postgresql set "$(ls /usr/lib/eselect-postgresql/slots/ -1v|tail -n 1)"
	fi
}
