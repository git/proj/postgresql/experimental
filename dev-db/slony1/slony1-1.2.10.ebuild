# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

WANT_POSTGRES_SLOTS="7.4 8.2"
inherit eutils versionator postgresql-ext

IUSE="perl"

DESCRIPTION="A replication system for the PostgreSQL Database Management System"
HOMEPAGE="http://slony.info/"
SRC_URI="http://main.slony.info/downloads/$(get_version_component_range 1-2)/source/${P}.tar.bz2"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86"

DEPEND="
	$(postgresql-ext_pg_slots_depend)
	perl? ( dev-perl/DBD-Pg )"

pgslot_src_compile() {
	# need to override sharedir for slots below 8.1
	local extraconf
	if [[ "${SLOTSLOT#7}" != "${SLOTSLOT}" ]] || [[ "${SLOTSLOT#8.0}" != "${SLOTSLOT}" ]] ; then
		extraconf="--with-pgsharedir=$(postgresql_get_datadir_for_slot $SLOTSLOT)"
	fi
	local bs="$(postgresql_get_bindir_for_slot $SLOTSLOT)"
	econf \
		--prefix="$(postgresql_get_prefix_for_slot $SLOTSLOT)" \
		--sysconfdir="/etc/slony1-$SLOTSLOT" \
		--with-pgconfigdir="$bs" \
		$(use_with perl perltools) \
		$extraconf \
		|| die "econf failed"
	emake || die "emake failed"

	if use perl ; then
		cd "${S}/tools"
		emake || die "emake in tools failed"
	fi
}

pgslot_src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodir /usr/lib/eselect-postgresql/slots/$SLOTSLOT
	cat >"${D}/usr/lib/eselect-postgresql/slots/${SLOT}/service" <<-__EOF__
		postgres_ebuilds="\${postgres_ebuilds} $PF"
		slony1_service="slony1-$SLOT"
	__EOF__

	newinitd "${FILESDIR}/slony1.init" slony1-$SLOTSLOT || die "failed to do init.d for slot $SLOTSLOT"
	dosed "s:@SLOTSLOT@:${SLOTSLOT}:g" /etc/init.d/slony1-$SLOTSLOT
	dosed "s:@bindir@:$(postgresql_get_bindir_for_slot $SLOTSLOT):g" /etc/init.d/slony1-$SLOTSLOT
	newconfd "${FILESDIR}/slony1.conf" slony1-$SLOTSLOT || die "failed to do conf.d for slot $SLOTSLOT"
	dosed "s:@postgres_service@:$(postgresql_get_service_for_slot $SLOTSLOT):g" /etc/conf.d/slony1-$SLOTSLOT
}

src_install() {
	multislot_src_install

	dodoc HISTORY-1.1 INSTALL README SAMPLE TODO UPGRADING doc/howto/*.txt
	dohtml doc/howto/*.html
}

pkg_postinst() {
	eselect postgresql update
}
