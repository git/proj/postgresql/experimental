# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools eutils multilib

KEYWORDS="~amd64 ~ppc ~x86"

DESCRIPTION="Geographic Objects for PostgreSQL."
HOMEPAGE="http://postgis.refractions.net"
SRC_URI="http://www.postgis.org/download/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
IUSE="geos proj"

RESTRICT="test"

RDEPEND=">=dev-db/postgresql-7.2
		geos? ( sci-libs/geos )
		proj? ( sci-libs/proj )"
DEPEND="${RDEPEND}
		app-text/docbook-xsl-stylesheets
		sys-devel/autoconf"

RESTRICT="test"

pkg_setup() {
	if has_version "<${CATEGORY}/${PF}" ; then
		ewarn "Don't forget to dump your databases with -Fc options before"
		ewarn "upgrading postgis."
		ewarn "(see http://postgis.refractions.net/docs/ch02.html#upgrading )"
		ebeep 4
	fi
}

src_unpack() {
	unpack ${A}
	cd "${S}"

	epatch "${FILESDIR}/${P}_doc.patch"

	local xslv
	xslv="$(ls /usr/share/sgml/docbook/ | grep xsl\-)"
	einfo "doc will be build with template:"
	einfo "${xslv}"
	sed "s:xsl-stylesheets:${xslv}:g" -i configure.in || die "xsl-stylesheets sed failed"
}

src_compile() {
	cd "${S}"

	eautoconf

	econf \
		$(use_with geos) \
		$(use_with proj) \
		--enable-autoconf \
		--datadir=/usr/share/postgresql/contrib/ \
		--libdir=/usr/$(get_libdir)/postgresql/ \
		--with-docdir=/usr/share/doc/${PF}/html/ \
		|| die "econf failed"

	emake || die "emake failed"

	emake docs || die "emake docs failed"

	cd "${S}/topology/"
	emake || die "emake topology failed"
}

src_install() {
	cd "${S}"
	dodir /usr/$(get_libdir)/postgresql/ /usr/share/postgresql/contrib/
	emake DESTDIR="${D}" install || die "emake install failed"

	cd "${S}/topology/"
	emake DESTDIR="${D}" install || die "emake install topology failed"

	cd "${S}"
	dodoc CHANGES COPYING CREDITS README.postgis TODO loader/README.* doc/*txt \
		|| die "Unable to install doc"

	docinto topology
	dodoc topology/{TODO,README}

	cd "${S}"
	emake DESTDIR="${D}" docs-install || die "emake docs-install failed"

	dobin utils/postgis_restore.pl
}

pkg_postinst() {
	einfo "To create your first postgis database use the following commands:"
	einfo " # su postgres"
	einfo " # createdb test"
	einfo " # createlang plpgsql test"
	einfo " # psql -d test -f /usr/share/postgresql/contrib/lwpostgis.sql"
	einfo " # psql -d test -f /usr/share/postgresql/contrib/spatial_ref_sys.sql"
	einfo "For more informations see: http://www.postgis.org/documentation.php"
	einfo "(For french users only see http://postgis.fr )"
}
