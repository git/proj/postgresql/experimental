# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit perl-module eutils einput

KEYWORDS="~x86 ~amd64 ~ppc"

DESCRIPTION="Perl bindings for OpenFTS, an advanced PostgreSQL-based search engine based on tsearch2."
HOMEPAGE="http://openfts.sourceforge.net/"
SRC_URI="http://jawed.name/dev/gentoo/${P}.tar.bz2"
LICENSE="GPL-2"
SLOT="0"
IUSE="doc sampledb snowball"

RESTRICT="mirror"

DEPEND=">=dev-db/postgresql-7.4
		>=dev-lang/perl-5.8
		>=dev-perl/DBI-1.4
		>=dev-perl/DBD-Pg-1.2
		>=perl-core/Time-HiRes-1.6"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	sed -i -e "s:/etc:${D}/etc:" Makefile.PL
	if ! useq snowball ; then
		find . -name "*nowball*.pm" -type f | xargs rm -f
	fi
}

src_install() {
	perl-module_src_install
	if ! useq sampledb ; then
		rm -f examples/apod-en.tar.gz
	fi
	if useq doc ; then
		dodoc doc/*
	fi
	dodoc examples/*
}

pkg_postinst() {
	einfo
	einfo "OpenFTS requires a tsearch2-able PostgreSQL database."
	einfo "To create a tsearch2-able database, run the following:"
	einfo
	einfo "$ createdb openfts"
	einfo "$ psql openfts < /usr/share/postgresql/contrib/tsearch2.sql"
	einfo
	if useq sampledb ; then
		einfo
		einfo "After creating the tsearch2 database, run:"
		einfo
		einfo "$ emerge --config ${PF}"
		einfo
		einfo "to finish indexing the sample database"
		einfo
	fi
}

extractFile() {
	cp $1/$2.gz $3
	gunzip -f $3/$2.gz
	einfo "Extracted and uncompressed $2 into $3"
}

pkg_config() {
	if built_with_use 'dev-db/openfts' sampledb ; then
		local DOCDIR="/usr/share/doc/${PF}"
		local TMPDIR="/tmp/openfts"
		einfo
		einfo "Starting config..."
		einfo
		einfo "Extracting apod documents into ${TMPDIR} ..."
		einfo
		epause 2
		mkdir -p "${TMPDIR}/apod"
		tar xzf "${DOCDIR}/apod-en.tar.gz" -C "${TMPDIR}"
		einput_question "Enter the OpenFTS database username" "postgres"
		local PGSQL_USER="${last_answer}"
		einput_question "Enter the OpenFTS database name" "openfts"
		local PGSQL_OFTS_DB="${last_answer}"
		einfo
		einfo "Copying necessary files ..."
		einfo
		extractFile "${DOCDIR}" "init.pl" "${TMPDIR}"
		extractFile "${DOCDIR}" "index.pl" "${TMPDIR}"
		extractFile "${DOCDIR}" "search.pl" "${TMPDIR}"
		einfo "This configuration will DROP the existing OpenFTS database on"
		einfo "${PGSQL_OFTS_DB} ... press CTRL+C now to stop this."
		einfo
		ebeep
		epause 5
		sed -i "s/DBI:Pg:/DBI:Pg:user=${PGSQL_USER};/" "${TMPDIR}/search.pl"
		sed -i "s/DBI:Pg:/DBI:Pg:user=${PGSQL_USER};/" "${TMPDIR}/index.pl"
		sed -i "s/DBI:Pg:/DBI:Pg:user=${PGSQL_USER};/" "${TMPDIR}/init.pl"
		perl "${TMPDIR}"/init.pl "${PGSQL_OFTS_DB}" drop
		einfo
		einfo "Creating INDICES ..."
		einfo
		epause 2
		perl "${TMPDIR}"/init.pl "${PGSQL_OFTS_DB}"
		einfo
		einfo "Indexing apod documents ..."
		einfo
		epause 2
		find "${TMPDIR}/apod" -type f | perl "${TMPDIR}/index.pl" openfts
		cp -f "${TMPDIR}/search.pl" "${DOCDIR}"
		einfo
		einfo "Finished indexing apod documents."
		einfo "Consult search.pl under /usr/share/doc/${PF} for usage."
		einfo
	else
		eerror
		eerror "You need to emerge dev-perl/openfts with the 'sampledb' USE flag"
		eerror "enabled in order to run 'emerge --config ${PF}' successfully."
		eerror
	fi
}
