# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils postgresql

KEYWORDS="~alpha ~amd64 ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86 ~x86-fbsd"

DESCRIPTION="C++ client API for PostgreSQL. The standard front-end for writing C++ programs that use PostgreSQL. Supersedes older libpq++ interface."
SRC_URI="ftp://thaiopensource.org/software/${PN}/${P}.tar.gz"
HOMEPAGE="http://thaiopensource.org/development/libpqxx/"
LICENSE="BSD"
SLOT="0"
IUSE=""

DEPEND="virtual/postgresql-base"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# should be safe enough to remove the lines directly from configure,
	# since it's copied directly from configure.ac
	sed -i \
		-e 's/\(gcc_visibility\)=yes/\1=no/g' \
		-e 's@\(#define PQXX_HAVE_GCC_VISIBILITY 1\)@/* \1 */@g' \
		-e '/-Werror/d' \
		configure || die "sed failed"
}

src_compile() {
	econf --enable-shared PATH="$(postgresql_get_bindir):${PATH}" || die "econf failed"
	emake || die "emake failed"
}

src_install () {
	emake DESTDIR="${D}" install || die "emake install failed"

	dodoc AUTHORS ChangeLog NEWS README* TODO
	dohtml -r doc/html/*
}

src_test() {
	ewarn "The tests need a running PostgreSQL server and an existing database!"
	ewarn "You can set the following environment variables to change the connection parameters:"
	ewarn "PGDATABASE (default: username, probably root)"
	ewarn "PGHOST (default: localhost)"
	ewarn "PGPORT (default: pg's UNIX domain-socket)"
	ewarn "PGUSER (default: username, probably root)"
	epause 10

	cd "${S}/test"
	emake -j1 check || die "emake check failed"
}
