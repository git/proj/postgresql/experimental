# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils postgresql
IUSE=""
DESCRIPTION="C++ client API for PostgreSQL. The standard front-end for writing C++ programs that use PostgreSQL. Supersedes older libpq++ interface."
SRC_URI="ftp://gborg.postgresql.org/pub/libpqxx/stable/${P}.tar.gz
	http://gborg.postgresql.org/download/libpqxx/stable/${P}.tar.gz"
HOMEPAGE="http://gborg.postgresql.org/project/libpqxx/projdisplay.php"

LICENSE="BSD"
KEYWORDS="~alpha ~amd64 ~ppc ~ppc64 ~sparc ~x86"
SLOT="0"

DEPEND="virtual/postgresql-base"

src_unpack() {
	unpack ${A}
	cd ${S}
	epatch ${FILESDIR}/${P}-namespace-fix.patch
}

src_compile() {
	econf --enable-shared PATH="$(postgresql_get_bindir):${PATH}" || die "econf failed"
	emake || die "emake failed"
}

src_install () {
	make DESTDIR=${D} install || die
	dodoc AUTHORS ChangeLog COPYING INSTALL NEWS README* TODO
	dohtml -r doc/html/*
}
